#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
/*
typedef struct Node{
    char * key;
    char * value;
    time_t timestamp; //needed to tell if a key expired. 
}Node;

typedef struct Tree{
    Node * root;
    Node * left;
    Node * right;
}Tree; 
*/
Node * create_Node(char *key, char *value, int expiry){
    Node * newnode;
    newnode = (Node *)calloc(1, sizeof(Node));
    newnode->key = (char *)calloc(strlen(key)+1, sizeof(char));
    newnode->value = (char *)calloc(strlen(value)+1, sizeof(char));
    strcpy(newnode->key, key);
    strcpy(newnode->value, value);
    gettimeofday(&(newnode->time), NULL);
    newnode->duration = expiry;
    return newnode;
}
Tree * init_tree(){
    Tree * tree = (Tree *)calloc(1, sizeof(Tree));
    tree->root = NULL;
    tree->left = NULL;
    tree->right = NULL;
    return tree;
}

Node * i_set_key(Tree ** t, char * key, char * value, int expiry){
    if(t == NULL){
        return NULL;
    }
    else if((*t)==NULL){
        Node * newnode = create_Node(key, value, expiry);
        (*t) = init_tree();
        (*t)->root = newnode;
        (*t)->left = NULL;
        (*t)->right = NULL;
        return newnode;
    }
    else if((*t)->root == NULL){
        (*t)->root = create_Node(key, value, expiry);
        return (*t)->root;
    }
    Tree ** aux = t;
    while(*aux && (*aux)->root != NULL){
        if(strcmp(key, (*aux)->root->key)==0){
            //If the keys are equal, check if said key expired
            struct timeval current_time; 
            struct timeval node_time = (*aux)->root->time;
            gettimeofday(&current_time, NULL);
            // printf("=== ELAPSED TIME: %ld ===\n", (current_time.tv_sec - node_time.tv_sec)); 
            int duration = (*aux)->root->duration;
            //If the node has an expire time and is expired
            if(duration>0 && (current_time.tv_sec - node_time.tv_sec)>duration){
                (*aux)->root->value = value;
                (*aux)->root->duration = expiry;
                gettimeofday(&((*aux)->root->time), NULL);
                return (*aux)->root;
            }
            else return NULL;
        }
        else if(strcmp(key, (*aux)->root->key)>0){
            aux = &((*aux)->right);
        }
        else aux = &((*aux)->left);
    }
    Node * newnode = create_Node(key, value, expiry);
    (*aux) = init_tree();
    (*aux)->root = newnode;
    (*aux)->left = NULL;
    (*aux)->right = NULL;
    return newnode;
}

Node * set_key(Tree ** t, char * key, char * value, int expiry){
    //t points to nothing
    if(t == NULL){
        return NULL;
    }
    //(*t) points to not initialised tree
    else if((*t)==NULL){
        Node * newnode = create_Node(key, value, expiry);
        (*t) = init_tree();
        (*t)->root = newnode;
        (*t)->left = NULL;
        (*t)->right = NULL;
        return newnode;
    }
    //(*t) points to a tree with no elements
    else if((*t)->root == NULL){
        (*t)->root = create_Node(key, value, expiry);
        return (*t)->root;
    }
    else if(strcmp(key, (*t)->root->key)==0){
        //If the keys are equal, check if said key expired
        struct timeval current_time; 
        struct timeval node_time = (*t)->root->time;
        gettimeofday(&current_time, NULL);
        // printf("=== ELAPSED TIME: %ld ===\n", (current_time.tv_sec - node_time.tv_sec)); 
        int duration = (*t)->root->duration;
        //If the node has an expire time and is expired
        if(duration>0 && (current_time.tv_sec - node_time.tv_sec)>duration){
            (*t)->root->value = value;
            (*t)->root->duration = expiry;
            gettimeofday(&((*t)->root->time), NULL);
            return (*t)->root;
        }
        else return NULL;
    }
    //If the new key is greater than the root, call the function on the rightmost tree
    else if(strcmp(key, (*t)->root->key)>0){
        return set_key(&((*t)->right), key, value, expiry);
    }
    else return set_key(&((*t)->left), key, value, expiry);
}
char * i_get_key(Tree * tr, char * key){
    Tree * t = tr;
    if(t == NULL || t->root == NULL){
        return NULL;
    }
    while(t && t->root){
        if(strcmp(t->root->key, key)==0){
            struct timeval current_time; 
            struct timeval node_time = t->root->time;
            gettimeofday(&current_time, NULL);
            // printf("=== ELAPSED TIME: %ld ===\n=== DURATION:%d ===", (current_time.tv_sec - node_time.tv_sec), t->root->duration); 
            fflush(stdout);
            int duration = t->root->duration;
            //If the node has an expire time and is not expired
            if(!(duration>0 && (current_time.tv_sec - node_time.tv_sec)>(duration))){
                return t->root->value;
            }
            else return NULL;
        }
        else if(strcmp(key, (t->root->key))>0){
            t = t->right;
        }
        else t = t->left;
    }
    return NULL;
    //If root and key are the same, check if key has not expired yet
}
char * get_key(Tree * t, char * key){
    //t points to nothing
    if(t == NULL || t->root == NULL){
        return NULL;
    }
    //If root and key are the same, check if key has not expired yet
    else if(strcmp(t->root->key, key)==0){

        struct timeval current_time; 
        struct timeval node_time = t->root->time;
        gettimeofday(&current_time, NULL);
        // printf("=== ELAPSED TIME: %ld ===\n=== DURATION:%d ===", (current_time.tv_sec - node_time.tv_sec), t->root->duration); 
        fflush(stdout);
        int duration = t->root->duration;
        //If the node has an expire time and is not expired
        if(!(duration>0 && (current_time.tv_sec - node_time.tv_sec)>(duration))){
            return t->root->value;
        }
        else return NULL;
    }
    //If key is greater than the root key, search on the rightmost tree
    else if(strcmp(key, (t->root->key))>0){
        return get_key(t->right, key);
    }
    else return get_key(t->left, key);
}
int * clean(Tree * t){
    //t is NULL: there's nothing to do
    if(t == NULL){
        return NULL;
    }
    else{
        int * left = NULL;
        int * right = NULL;
        if(t->left!=NULL){
            left = clean(t->left);
        }
        if(t->right!=NULL){
            right = clean(t->right);
        }
        //If left and right clean were successful, free the current node
        if(left==NULL && right==NULL){
            free(t->root->key);
            free(t->root->value);
            free(t->root);
            free(t);
            return NULL;
        }
        else{
            fprintf(stderr, "Tree clean was not successful!");
            int * rvalue = (int *)calloc(1, sizeof(int));
            *rvalue = EXIT_FAILURE;
            return rvalue;
        }
    }
}
void print_tree(Tree *t){
    if(t == NULL){
        printf("()");
    } 
    else{
        printf("(%s:%s", t->root->key, t->root->value);
        print_tree(t->left);
        print_tree(t->right);
        printf(")");
    }
}

void testFunctions(){
    char key_buffer[256] = {0};
    char value_buffer[256] = {0};
    char expiry[64] = {0};
    Tree * t = init_tree();
    do{
        puts("Specify the key");
        fgets(key_buffer, 255, stdin);
        puts("Specify the value");
        fgets(value_buffer, 255, stdin);
        key_buffer[strlen(key_buffer)-1]=0;
        value_buffer[strlen(value_buffer)-1]=0;
        puts("Specify an expire time: insert 'x' to stop");
        fgets(expiry, 63, stdin);
        if(expiry[0]=='x'){
            break;
        }
        i_set_key(&t, key_buffer, value_buffer, atoi(expiry));
        printf("Current tree: ");
        print_tree(t);
        printf("\n");
    }while(1);
    puts("freeing tree");
    clean(t);
    return;
}

/*
int main(){
    testFunctions();
    return EXIT_SUCCESS;
}
*/
