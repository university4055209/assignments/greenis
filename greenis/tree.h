#include <stdint.h>
#include <sys/time.h>

#ifndef TREE_H
#define TREE_H

typedef struct Node{
    char * key;
    char * value;
    struct timeval time; //needed to tell if a key expired. 
    int duration;
}Node;

typedef struct Tree{
    Node * root;
    struct Tree * left;
    struct Tree * right;
}Tree; 

//Utilities
Tree * init_tree(); //creates an empty tree
Node * create_Node(char * key, char * value, int expiry);
Node * set_key(Tree ** t, char * key, char * value, int expiry); //Adds a new node, returns the created node, NULL if the node exists
Node * i_set_key(Tree ** t, char * key, char * value, int expiry); //Adds a new node, returns the created node, NULL if the node exists
char * get_key(Tree * t, char * key); //Returns the key value, NULL if the key does not exist
char * i_get_key(Tree * t, char * key); //Returns the key value, NULL if the key does not exist
int * clean(Tree * t); //free the allocated space, returns NULL on success
void print_tree(Tree * t);

#endif //TREE_H
