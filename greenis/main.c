#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <poll.h>
#include "tree.h"

typedef struct t_args{
    int client_socket;
    struct sockaddr_in client_addr;
    socklen_t client_len;
}t_args;

void exit_with_error(const char * msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

// https://redis.io/docs/reference/protocol-spec/
#define PORT 7379 
#define QUEUE 3
#define MAGIC 1000000
#define SET_CMD "SET"
#define GET_CMD "GET"
#define CMD_SUCCESS "+OK\r\n"
#define CMD_FAILURE "$-1\r\n"

void * handle_command(Tree ** database, char ** command, int command_len){
    char * cmd_ret = NULL;
    printf("Received command of size %d\n", command_len);
    for(int i = 0; i<command_len; i++){
        printf("Command[%d] = %s END\n", i, command[i]); 
    }
    if(strcmp(command[0], SET_CMD)==0){  
        Node * ret;
        //EX is specified
        if(command_len == 5){
            ret = i_set_key(database, command[1], command[2], atoi(command[4]));
        }
        else if(command_len == 3){
            ret = i_set_key(database, command[1], command[2], -1);
        }
        else{
            fprintf(stderr, "ERROR: command not recognised");
            ret = NULL;
        }
        if(ret != NULL){
            cmd_ret = (char *)calloc(strlen(CMD_SUCCESS)+1, sizeof(char));
            strcpy(cmd_ret, CMD_SUCCESS);
        }
        else{
            cmd_ret = (char *)calloc(strlen(CMD_FAILURE)+1, sizeof(char));
            strcpy(cmd_ret, CMD_FAILURE);
        }
    }
    else if(strcmp(command[0], GET_CMD)==0){
        char * ret;
        if(command_len == 2){
            ret = i_get_key(*database, command[1]); 
        }
        else{
            fprintf(stderr, "ERROR: command not recognised");
            ret = NULL;
        }
        if(ret != NULL){
            //10 bytes for padding ($ sign plus strlen plus \r\n...)
            cmd_ret = (char *)calloc(strlen(ret)+10, sizeof(char));
            sprintf(cmd_ret, "$%lu\r\n%s\r\n", strlen(ret), ret);
        }
        else{
            cmd_ret = (char *)calloc(strlen(CMD_FAILURE)+1, sizeof(char));
            strcpy(cmd_ret, CMD_FAILURE);
        }
    }
    else{
        fprintf(stderr, "ERROR: command format not valid");
        cmd_ret = (char *)calloc(strlen(CMD_FAILURE)+1, sizeof(char));
        strcpy(cmd_ret, CMD_FAILURE);
    }
    return cmd_ret;
}

void * routine(void * arg){
    int ret = 0;
    Tree * database = init_tree();
    t_args * args = (t_args *)arg;
    char buffer[512];
    struct pollfd p = {0};
    p.fd = args->client_socket; 
    p.events = POLLIN;
    //poll on client socket for 30 seconds
    while(1){
        puts("Waiting for command");
        fflush(stdout);
        ret = poll(&p, 1, -1);
        if(!ret){
            //Since poll timer is disabled this will never happen
            fprintf(stderr, "connection timed out!");
            close(args->client_socket);
            free(args);
            pthread_exit(NULL);
        }
        else if(ret == -1){
            exit_with_error("poll error");
        }
        while(1){
            //From the manpage: The receive calls normally
            // return any data available, up to the requested amount, rather
            // than waiting for receipt of the full amount requested
            // assuming no message is longer than 
            ret = recv(args->client_socket, buffer, sizeof(buffer), 0); 
            if(ret == -1){
                if(errno == EINTR){
                    continue;
                }
                exit_with_error("recv error");
            }
            if(strlen(buffer)==0){
                puts("EOF reached, closing connection");
                pthread_exit(NULL);
            }
            //printf("=== Received : %s ===", buffer);
            char * token = strtok(buffer, "\r\n");
            char ** command;
            int pos = 0;
            int maxpos = MAGIC;
            while(token != NULL && pos<maxpos){
                //printf("token#%d:%s\n", pos, token);
                char first = *token;
                //The lenght if the array
                if(first == '*'){
                    int arr_size = atoi(token+1);
                    maxpos = arr_size;
                    if(arr_size == 0){
                        exit_with_error("conversion error");
                    }
                    command = (char **)calloc(arr_size, sizeof(char *)); 
                }
                //The string lenght
                else if(first == '$'){
                    int string_size = atoi(token+1);
                    if(string_size == 0){
                        exit_with_error("conversion error");
                    }
                    command[pos] = (char *)calloc(string_size+1, sizeof(char));
                }
                //This is the string itself
                else{
                    //puts("Handling string");
                    int token_pos = 0;
                    do{
                        //printf("token_pos = %d, ", token_pos);
                        fflush(stdout);
                        (command[pos])[token_pos] = token[token_pos];
                        token_pos++;
                    }while(token[token_pos]!='\0');
                    //printf("\n");
                    pos++;
                }
                token = strtok(NULL, "\r\n");
            }
            char * c_ret = handle_command(&database, command, pos);
            printf("handle returned: %s\n", c_ret);
            // ================ TODO ==========================================
            int bytes_sent = 0;
            int msg_size = strlen(c_ret);
            puts("sending reply");
            while(bytes_sent != msg_size){
                ret = send(args->client_socket, c_ret+bytes_sent, msg_size-bytes_sent, 0);
                if(ret == -1){
                    if(errno == EINTR){ continue; }
                    exit_with_error("send error");
                }
                bytes_sent += ret;
            }
            puts("sent");
            for(int i = 0; i<pos; i++){
                free(command[i]);
            }
            free(command);
            //insert command free, check that handle works properly, send response 
            memset(buffer, 0, sizeof(buffer));
        }
    }
    pthread_exit(NULL);
}

int main(int argc, const char * argv[]) {
    
    int ret = 0;
    // Open Socket and receive connections
    int welcome_sock = socket(AF_INET, SOCK_STREAM, 0);
    if(welcome_sock==-1){
        exit_with_error("socket error");
    }
    struct sockaddr_in welcome_addr = {0};
    welcome_addr.sin_family = AF_INET;
    welcome_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    welcome_addr.sin_port = htons(PORT);
    socklen_t addr_len = sizeof(struct sockaddr_in);

    ret = bind(welcome_sock, (struct sockaddr *)&welcome_addr, addr_len);
    if(ret == -1){
        exit_with_error("bind error");
    }

    ret = listen(welcome_sock, QUEUE);
    if(ret == -1){
        exit_with_error("listen error");
    }

    int client_socket;
    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(struct sockaddr_in);

    pthread_attr_t attr; 
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    while(1){
        client_socket = accept(welcome_sock, (struct sockaddr *)&client_addr, &client_len); 
        if(client_socket == -1) {
            exit_with_error("accept error");
        }
        t_args * args = (t_args *)calloc(1, sizeof(t_args));
        args->client_socket = client_socket;
        args->client_len = client_len;
        args->client_addr = client_addr;
        pthread_t t;
        ret = pthread_create(&t, &attr, routine, args);
        if(ret){
            errno = ret;
            perror("pthread creation error");
        }
    }

    pthread_attr_destroy(&attr);
    // Keep a key, value store (you are free to use any data structure you want)

    // Create a process for each connection to serve set and get requested

}

