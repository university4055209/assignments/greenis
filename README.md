# greenis 

## resources

+ For protocol information, refer to [the specifications](https://redis.io/docs/reference/protocol-spec/);
+ Refer to [this section](https://redis.io/docs/reference/clients/) for setting up the tcp connections; 
+ Here is the [get](https://redis.io/commands/get/) and [set](https://redis.io/commands/set/) specifications;
+ [this](https://github.com/SENSES-Lab-Sapienza/Lab_Reti/tree/aa_2023_24/Resources/greenis) is the starting repo;
+ If you want to implement a graceful termination, refer to the [signal handling section](https://redis.io/docs/reference/signals/).

## Solution sketch

For the sake of the exercice we can expect the commands received from the client to always be arrays of strings 
```shell
# array notation
*<number-of-elements>\r\n<element-1>...<element-n>
# given that the array contains bulk strings
*<number-of-elements>\r\n$<length1>\r\n<string1>\r\n...$<lengthn>\r\n<datan>\r\n
```
We can then implement the following parsing strategy, given an established connection:
1. Parse the array lenght (read up to \r\n). Let the lenght be $x$: allocate `x*sizeof(char *)` bytes. 
2. For each element of the array, let $s_i$ be the size of the string in the ith position: allocate `s_i * sizeof(char)` bytes. 
3. Having executed the command, free all the allocated space. 

## Implementation 

Here we present an abstraction for the implementation. 

### Connection creation 

The first step is to create a socket
```c
//instantiate a tcp socket
int socket(AF_INET, int SOCK_DGRAM, 0) 
```
Having created a socket, we now need to `bind()` it to an address, specify how many connections we want to be waiting on `listen()` and `accept()` them as they arrive. Since the implementation is multi-threaded, each accepted connection is handed over to a thread via argument passing. Instead of using the posix functionality of keys and values, which would require some form of access control over the socket that's used to accept a connection, it is considered better to just allocate a `int * arg` to pass on with `pthread_create`. It is leaved to the thread itself to handle the free of its arguments.  
After the creation, the thread is immediately detached (`pthread_detach()`). As stated in the documentation, the opened connections should remain on idle as long as the client chooses to close it. Since the `client.py` file is missing a close request, we configure the threads to close their connection after no message has been received in 30 seconds. To do that we use the `poll()` function defined in the [poll header](https://man7.org/linux/man-pages/man0/poll.h.0p.html) ([here](https://linux.die.net/man/2/poll) is another description of how polling works).

### `GET` and `SET` commands

The [get specifications](https://redis.io/commands/get/) are straight-forward and require no particular explaination.  
The [set command](https://redis.io/commands/set/) has various options: here we assume that the `EX` option is the only one that could be specified, since that's the only option that `client.py` effectively requests. To handle expiry of keys `sys/time.h` and its function `gettimeofday()` are used. See [the manpage](https://man7.org/linux/man-pages/man2/gettimeofday.2.html).
In order to achieve a $O(1)$ complexity it would be necessary to implement an effecient hashing function. To keep things simple, here we use a binary tree to compromise between efficiency and simplicity.

```c
typedef struct Node{
    char * key;
    char * value;
    struct timeval time; //needed to tell if a key expired. 
    int duration;
}Node;

typedef struct Tree{
    Node * root;
    struct Tree * left;
    struct Tree * right;
}Tree; 

//Utilities
Tree * init_tree(); //creates an empty tree
Node * create_Node(char * key, char * value, int expiry); //helper function 
Node * set_key(Tree ** t, char * key, char * value, int expiry); //Adds a new node, returns the created node, NULL if the node exists
char * get_key(Tree * t, char * key); //Returns the key value, NULL if the key does not exist
int * clean(Tree * t); //free the allocated space, returns NULL on success
void print_tree(Tree * t); //prints the tree using brackets notation
```



